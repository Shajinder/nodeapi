const mongoose = require('mongoose');

const { Schema } = mongoose; // definir un nuevo schema.

// Definimos el modelo del Product.
const ProductSchema = Schema({
  name: {
    type: String,
    required: true,
    validate: {
      validator(v) {
        return v.length <= 30;
      },
      message:
        'El nombre solo puede tener una longitud maxima de 30 caracteres.'
    }
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    validate: {
      validator(v) {
        return v.length <= 255;
      },
      message: 'La descripción no puede superar los 255 carácteres.'
    }
  },
  created_at: {
    type: String
  },
  family: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Family',
    required: true
  }
});

// para poder usar el modelo product hay que exportalo.
module.exports = mongoose.model('Product', ProductSchema);
