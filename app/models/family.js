const mongoose = require('mongoose');

const { Schema } = mongoose; // definir un nuevo schema.

// Definimos el modelo del Family.
const FamilySchema = Schema({
  code: {
    type: String,
    required: true,
    validate: {
      validator(text) {
        return text.length <= 4;
      },
      message: 'El codigo solo puede tener una longitud maxima de 4 caracteres.'
    }
  },
  name: {
    type: String,
    required: true,
    validate: {
      validator(v) {
        return v.length <= 20;
      },
      message:
        'El nombre solo puede tener una longitud maxima de 20 caracteres.'
    }
  }
});

// para poder usar el modelo family hay que exportalo.
module.exports = mongoose.model('Family', FamilySchema);
