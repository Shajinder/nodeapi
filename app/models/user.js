const mongoose = require('mongoose');

const { Schema } = mongoose; // definir un nuevo schema.

// Definimos el modelo del Family.
const UserSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true // lo guardará en minúsculas
  },
  name: String,
  password: { type: String, select: false },
  date: { type: Date, default: Date.now() },
  lastLogin: Date
});

// para poder usar el modelo family hay que exportalo.
module.exports = mongoose.model('User', UserSchema);
