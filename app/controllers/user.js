const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs'); // la usaremos para encriptar la contrasenya.
const User = require('../models/user');
const servicejwt = require('../services/jwt'); // importamos la clase para generar el token del usuario.

function register(req, res) {
  const params = req.body;
  const user = new User();

  user.name = params.name;
  user.email = params.email;

  // si existe un usuario en la bases datos con el email o nombre usuario, este no se almacena.
  User.find({ email: user.email }).exec((err, users) => {
    if (err)
      return res
        .status(500)
        .send({ message: 'Error a la petición del usuario.' });

    if (users && users.length >= 1) {
      return res.status(200).send({
        message: 'Ya existe un usuario con el mismo email o usuario.'
      });
    }
    // si no existe el usuario a la base de datos lo almacena.
    bcrypt.genSalt(10, (err, salt) => {
      if (err)
        return res.status(500).send({ message: 'Error al generar la sal.' });

      bcrypt.hash(params.password, salt, null, (err, hash) => {
        user.password = hash;
        user.save((err, userStored) => {
          if (err)
            return res
              .status(500)
              .send({ message: 'Error al guardar el usuario.' });
          if (userStored) {
            // para que no muestra la contraseña en response.
            userStored.password = undefined;
            res.status(200).send({ user: userStored });
          } else {
            res.status(404).send({
              message: 'No se ha registrado el usuario correctamente.'
            });
          }
        });
      });
    });
  });
}

function login(req, res) {
  const params = req.body;

  const { email } = params;
  const { password } = params;
  // eslint-disable-next-line prettier/prettier

  // eslint-disable-next-line prettier/prettier
  User.findOne({"email":email }).select("+password").exec( (err, user) => {
      console.log('sha');
      console.log(user);
      if (err)
        return res.status(500).send({ message: 'Error en la petición.' });

      if (user) {
        bcrypt.compare(password, user.password, (err, data) => {
          if (data) {
            // si existe un usuario se hará el login
            // generamos el token si el parametro gettoken tiene algun valor en el body
            if (params.gettoken) {
              return res.status(200).send({
                token: servicejwt.createToken(user)
              });
            }

            // else
            user.password = undefined;
            return res.status(200).send({ user });
          }

          console.log(data);
          console.log(email);

          // else
          return res.status(404).send({
            message: 'Error no existe un usuario con este email y contraseña.'
          });
        });
      } else {
        return res
          .status(404)
          .send({ message: 'El usuario no se ha podido identificar.' });
      }
    });
}

module.exports = { register, login };
