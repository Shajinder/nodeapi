const fs = require('fs'); // nos permite trabajar con archivos.(librerias)
const path = require('path'); // nos permite trabajar con las rutas del sistema de ficheros.(librerias)
const moment = require('moment'); // para manejar el tiempo.
const Product = require('../models/product'); // cargamos la clase Product.

// test
function home(req, res) {
  res.status(200).send({
    message: 'Hola a mundo desde producto.'
  });
}

function list(req, res) {
  const find = Product.find();

  find.populate('family').exec((err, products) => {
    if (err)
      return res
        .status(500)
        .send({ message: 'Error al encontrar los productos.' });

    if (!products) return res.status(404).send({ message: err });

    return res.status(200).send({ products });
  });
}

function create(req, res) {
  const product = new Product(req.body);
  product.created_at = moment().unix();
  product.save((err, productStored) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar el product.',
        error: err
      });
    }
    return res.status(201).json(productStored);
  });
}

function edit(req, res) {
  const { id } = req.params;
  const update = req.body;

  Product.findByIdAndUpdate(
    id,
    update,
    { new: true },
    (err, productUpdated) => {
      if (err)
        return res.status(500).send({ message: 'Error en la petición.' });

      if (!productUpdated)
        return res
          .status(404)
          .send({ message: 'No se ha podido actualizar la product.' });

      return res.status(200).send({ product: productUpdated });
    }
  );
}

function show(req, res) {
  const { id } = req.params;

  const find = Product.findById(id);

  find.populate('family').exec((err, product) => {
    if (err)
      return res
        .status(500)
        .send({ message: 'Error al encontrar los productos.' });

    if (!product) return res.status(404).send({ message: err });

    return res.status(200).send({ product });
  });
}

function deleteProduct(req, res) {
  const { id } = req.params;

  Product.deleteOne({ _id: id }, (err, productDeleted) => {
    if (err)
      return res.status(500).send({ message: 'Error al borrar la product.' });

    if (!productDeleted)
      return res.status(404).send({ message: 'No se ha borrado la product.' });

    return res.status(200).send({ product: productDeleted });
  });
}
module.exports = {
  list,
  home,
  create,
  edit,
  show,
  deleteProduct
};
