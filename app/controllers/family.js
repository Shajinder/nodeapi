const fs = require('fs'); // nos permite trabajar con archivos.(librerias)
const path = require('path'); // nos permite trabajar con las rutas del sistema de ficheros.(librerias)
const Family = require('../models/family'); // cargamos la clase Family.

function home(req, res) {
  res.status(200).send({
    message: `Hola a mundo.${Date.now()}`
  });
}

function list(req, res) {
  Family.find((err, families) => {
    if (err) {
      return res.status(500).json({
        message: 'Error al listar.'
      });
    }
    return res.json(families);
  });
}

function create(req, res) {
  const family = new Family(req.body);
  family.save((err, familyStored) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar la family.',
        error: err
      });
    }
    return res.status(201).json(familyStored);
  });
}

function edit(req, res) {
  const familyId = req.params.id;
  const update = req.body;

  Family.findByIdAndUpdate(
    familyId,
    update,
    { new: true },
    (err, familyUpdated) => {
      if (err)
        return res.status(500).send({ message: 'Error en la petición.' });

      if (!familyUpdated)
        return res
          .status(404)
          .send({ message: 'No se ha podido actualizar la family.' });

      return res.status(200).send({ family: familyUpdated });
    }
  );
}

function show(req, res) {
  const { id } = req.params;

  Family.findById(id, (err, family) => {
    if (err) return res.status(500).send({ message: 'Error en la petición.' });

    if (!family)
      return res.status(404).send({ message: 'La familía no existe.' });

    return res.status(200).send({ family });
  });
}

function deleteFamily(req, res) {
  const { id } = req.params;

  Family.deleteOne({ _id: id }, (err, familyDeleted) => {
    if (err)
      return res.status(500).send({ message: 'Error al borrar la family.' });

    if (!familyDeleted)
      return res.status(404).send({ message: 'No se ha borrado la family.' });

    return res.status(200).send({ family: familyDeleted });
  });
}
module.exports = {
  list,
  home,
  create,
  edit,
  show,
  deleteFamily
};
