/* Cargamos todos los paquetes y librerias. */

/* Express framework nos proporciona funcionalidades como el  enrutamiento, 
opciones para gestionar sesiones y cookies... */
const express = require('express');
/* Body parse permite convertir el JSON en un objeto de JS. */
const bodyParser = require('body-parser');

// cargar el framework
const app = express();

// cargar rutas
const familyRoutes = require('./routes/family');
const productRoutes = require('./routes/product');
const userRoutes = require('./routes/user');

app.use(bodyParser.urlencoded({ extended: false })); // configuracion necesaria para el funcionamiento del bodyParser.
app.use(bodyParser.json()); // cuando recibe informacion en formato JSON, automaticamente lo convertira un objeto.

// rutas
app.use('/api/', familyRoutes);
app.use('/api/', productRoutes);
app.use('/api/', userRoutes);
// exportar todo lo incule la app.
module.exports = app;
