const express = require('express');
const FamilyController = require('../controllers/family');

// Api para tener acceso a los metodos get, delete, post...
const api = express.Router();

api.get('/family/home', FamilyController.home);
api.get('/family', FamilyController.list);
api.post('/family/create', FamilyController.create);
api.put('/family/edit/:id', FamilyController.edit);
api.get('/family/show/:id', FamilyController.show);
api.delete('/family/delete/:id', FamilyController.deleteFamily);

// Para poder exportar los metodos.
module.exports = api;
