const express = require('express');
const UserController = require('../controllers/user');

// Api para tener acceso a los metodos get, delete, post...
const api = express.Router();

api.post('/user/login', UserController.login);
api.post('/user/register', UserController.register);
// Para poder exportar los metodos.
module.exports = api;
