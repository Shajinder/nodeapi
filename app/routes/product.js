const express = require('express');
const ProductController = require('../controllers/product');
const mdAuth = require('../middlewares/auth');

// Api para tener acceso a los metodos get, delete, post...
const api = express.Router();

api.get('/product', mdAuth.ensureAuth, ProductController.list);
api.post('/product/create', mdAuth.ensureAuth, ProductController.create);
api.put('/product/edit/:id', mdAuth.ensureAuth, ProductController.edit);
api.get('/product/show/:id', mdAuth.ensureAuth, ProductController.show);
api.delete(
  '/product/delete/:id',
  mdAuth.ensureAuth,
  ProductController.deleteProduct
);

// Para poder exportar los metodos.
module.exports = api;
