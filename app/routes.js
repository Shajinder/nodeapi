const router = require('express').Router();
// const cervezas = require('./routes/cervezas.js');
const routerCervezasV2 = require('./routes/v2/cervezas.js');

router.use('/v2/cervezas', routerCervezasV2);

// router.use('/cervezas', cervezas);
router.get('/', (req, res) => {
  res.json({ mensaje: 'Bienvenido a nustra API!' });
});
module.exports = router;
