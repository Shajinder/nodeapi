const mysql = require('mysql2');

const connection = mysql.createConnection({
  user: 'root',
  password: 'password',
  host: 'localhost',
  database: 'cervezas'
});
module.exports = connection;
