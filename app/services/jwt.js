const jwt = require('jwt-simple'); // módulo Json Web Token
const moment = require('moment');
// módulo para fechas
const secret = 'clave_secreta_trabajo_de_rafa_shajinder_singh';

function createToken(user) {
  const payload = {
    sub: user._id, // no es muy seguro pero lo simplificamos así
    email: user.email,
    iat: moment().unix(), // fecha creación
    exp: moment()
      .add(30, 'days')
      .unix() // valido 1 mes
  };

  return jwt.encode(payload, secret);
}

module.exports = { createToken };
