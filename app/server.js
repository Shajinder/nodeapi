const express = require('express');
// llamamos a Express
const app = express();
const Cerveza = require('./models/vs/Cerveza');
require('./config/db');
const bodyParser = require('body-parser');
const router = require('./routes');

const port = process.env.PORT || 8080; // establecemos nuestro puerto
const miCerveza = new Cerveza({ nombre: 'Ambar' });
miCerveza.save((err, miCerveza) => {
  if (err) return console.error(err);
  console.log(`Guardada en bbdd ${miCerveza.name}`);
});

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.json({ mensaje: '¡Hola Mundo!' });
});

app.use('/api', router);

// iniciamos nuestro servidor
app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
